import React from 'react';
import { FunctionComponent, useState, useEffect } from 'react';

import './App.css';
import AWS from 'aws-sdk';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { NavBar } from './components/header/NavBar';
import HomePage from './components/home/Home';
import LoginPageContainer from './components/auth/login/LoginPageContainer';
import { SecretComponent } from './components/secret/SecretComponent';
import { Register } from './components/auth/Register';
import { PrivateRoute } from './components/routing/PrivateRoute';
import { CognitoUser } from 'amazon-cognito-identity-js';
import Footer from './components/footer/Footer';
import { FileUpload } from './components/FileUpload';
import { InfoComponent } from './components/info/Info';
import { UserManagementContainer } from './components/userManagement/UserManagementContainer';
import { authenticationService } from './services/authentication.service';

AWS.config.update({
  region: 'us-east-2',
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-2:bf68d7a2-de97-45b7-8f16-1cfaec1efcec',
  })
});

export const App: FunctionComponent<{}> = (props) => {

  const [authenticating, setAuth] = useState(false)
  const [loggedIn, setLoggedIn] = useState(false)
  const [currentUser, setUser] = useState(null)
  const [userRole, setRole] = useState('')

  useEffect(() => {
    // Issue with multiple auth calls. Creates 2 subscriptions which cause 2 calls one being not logged <in className=""></in>
    authenticationService.updateLoggedInUser();
    authenticationService.currentUser.subscribe((user: CognitoUser | undefined) => {

      if (user === undefined) {
        setRole('');
        setLoggedIn(false);
      } else {
        setLoggedIn(true);
        authenticationService.getUserRole().then((role: string) => {
          setRole(role);
        });
      }
    })
  });

  function setAuthenticating(isAuthenticating: boolean): void {
    setAuth(isAuthenticating);
  }

  function signIn(username: string, password: string): void {
    setLoggedIn(true);
  }

  function register(username: string): void {
    setLoggedIn(true);
  }

  function handleLogout(): void {
    authenticationService.logout();
  }

  return (
    <div className="App" >
      <Router>
        <NavBar userRole={userRole} loggedIn={loggedIn} handleLogout={handleLogout} user={currentUser} />

        <Route path="/" exact={true} render={() => <HomePage userRole={userRole} />} />

        <Route path="/info" component={InfoComponent} />

        <PrivateRoute
          isLoggedIn={!loggedIn}
          props={{ onSignIn: signIn, setAuthenticating, isAuthenticating: authenticating }}
          component={LoginPageContainer}
          redirectUrl="/"
          path="/login"
          requiredRole=""
          currentRole={userRole}
        />

        <PrivateRoute
          isLoggedIn={loggedIn}
          props={{}}
          component={SecretComponent}
          redirectUrl="/login"
          path="/secret"
          requiredRole="secret:visit"
          currentRole={userRole}
        />

        <PrivateRoute
          isLoggedIn={!loggedIn}
          props={{ onRegister: register }}
          component={Register}
          redirectUrl="/"
          path="/register"
          requiredRole=""
          currentRole={userRole}
        />

        <PrivateRoute
          isLoggedIn={loggedIn}
          props={{}}
          component={FileUpload}
          redirectUrl="/login"
          path="/fileUpload"
          requiredRole="fileUpload:visit"
          currentRole={userRole}
        />

        <PrivateRoute
          isLoggedIn={!loggedIn}
          props={{}}
          component={UserManagementContainer}
          redirectUrl="/"
          path="/user-management"
          requiredRole="user-management:visit"
          currentRole={userRole}
        />
      </Router>

      <Footer />
    </div>
  );
}

export default App;


