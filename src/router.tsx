import * as React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { App } from './App';

export const AppRouter: React.StatelessComponent<{}> = () => {
    return (
        <HashRouter>
            <div className="container-fluid">
                <Route component={App} />
                <Switch>
                </Switch>
            </div>
        </HashRouter>
    )
}