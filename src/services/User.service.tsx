import { UserData } from '../components/userManagement/models/User';
export const userService = {
    getAllUsers
}


function getAllUsers(): Promise<UserData[]> {
    return fetch('http://localhost:8080/users').then(function (response) {
        return response.json().then(function (data: UserData[]) {
            return data;
        });
    })
}