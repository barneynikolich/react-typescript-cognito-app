import { BehaviorSubject } from 'rxjs';

import { Auth } from 'aws-amplify';
import { CognitoUserSession, CognitoUser } from 'amazon-cognito-identity-js';

const currentUserSubject = new BehaviorSubject<CognitoUser | undefined>(undefined);

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    currentUserValue: currentUserSubject.value,
    updateLoggedInUser,
    getUserRole
};

function updateLoggedInUser(): Promise<CognitoUser | undefined> {
    return Auth.currentSession()
        .then((userSession: CognitoUserSession) => {
            return Auth.currentAuthenticatedUser()
                .then((user: CognitoUser) => {
                    currentUserSubject.next(user);
                    return user;
                })
        })
        .catch((error) => {
            currentUserSubject.next(undefined);
            return undefined;
        })
}

function getUserRole(): Promise<string> {
    return Auth.currentSession()
        .then((userSession: CognitoUserSession) => {
            const userRole = userSession.getIdToken().decodePayload();
            return userRole['cognito:groups'][0];
        })
        .catch((error) => {
            return '';
        });
}

async function login(username: string, password: string): Promise<CognitoUser> {
    return await Auth.signIn({ username, password, })
        .then((user: CognitoUser) => {
            currentUserSubject.next(user);
            return user;
        });
}

function logout(): void {
    // remove user from local storage to log user out
    // localStorage.removeItem('currentUser');
    Auth.signOut();
    // this.setState({ loggedIn: false, username: '' });
    currentUserSubject.next(undefined);

}