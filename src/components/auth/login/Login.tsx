import { Button, TextField } from "@material-ui/core";
import * as React from 'react';
import IUser from '../../models/User.interface'
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';


interface Props {
    user: IUser;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onLogin: (event: React.FormEvent) => void;
    error: any,
    isAuthenticating: boolean;
}

const LoginForm: React.StatelessComponent<Props> = (props) => {
    return (
        <form className="form-container" onSubmit={props.onLogin}>
            <LockIcon color="primary" fontSize="large" />
            <h1>Sign in</h1>
            <div className="input-fields-container">
                <TextField
                    id="username"
                    type="text"
                    label="Username"
                    margin="dense"
                    variant="outlined"
                    name="username"
                    value={props.user.username}
                    onChange={props.onChange}
                />

                <TextField
                    id='password'
                    label="Password"
                    type="password"
                    margin="dense"
                    variant="outlined"
                    name="password"
                    value={props.user.password}
                    onChange={props.onChange}
                />

                <Button type="submit" id="login-button" variant="contained" color="primary">
                    Login
                </Button>
                <br />
                <Link to="/register" style={{ textDecoration: 'none', color: 'grey', marginRight: '20px' }}>
                    <Typography variant="h5">
                        Not a member? Sign up
                    </Typography>
                </Link>
                <p id="copyright-text"> Copyright © Built by barn with Material-UI.</p>

                <div>
                    {
                        props.isAuthenticating &&
                        <Typography variant="h6" style={{ color: 'green' }} >
                            Logging you in...
                        </Typography>
                    }

                    {
                        props.error ?
                            <Typography variant="h6" style={{ color: 'red' }} >
                                {props.error}
                            </Typography> : undefined

                    }
                </div>
            </div>
        </form >
    );
};

export default LoginForm;
