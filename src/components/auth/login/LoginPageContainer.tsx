import Container from '@material-ui/core/Container';
import * as React from 'react';
import LoginForm from './Login';
import './Login.css';
import { CognitoUser } from '@aws-amplify/auth';
import { authenticationService } from '../../../services/authentication.service';
import { FunctionComponent, useState } from 'react';

interface Props {
    onSignIn: (username: string) => void;
    setAuthenticating: (isAuthenticting: boolean) => void;
    isAuthenticating: boolean;
}

const LoginPageContainer: FunctionComponent<Props> = (props) => {
    const [user, setUser] = useState({ username: '', password: '' });
    const [errors, setErrors] = useState({ cognito: null, passwordMatch: false });

    function onFieldValueChange(event: React.ChangeEvent<HTMLInputElement>) {
        const fieldName: string = event.target.name;
        const nextState = {
            ...user,
            [fieldName]: event.target.value,
        };
        setUser(nextState);
    }

    function onSubmit(event: React.FormEvent) {
        event.preventDefault();

        props.setAuthenticating(true);
        authenticationService.login(user.username, user.password).then((cognitoUser: CognitoUser) => {
            props.onSignIn(user.username);
            props.setAuthenticating(false);
        }).catch((error => {
            setErrors({ cognito: error.message, passwordMatch: false })
            props.setAuthenticating(false);
        }));
    }

    return (
        <Container className='login-container' maxWidth="sm">
            <LoginForm
                user={user}
                onChange={onFieldValueChange}
                onLogin={onSubmit}
                error={errors.cognito}
                isAuthenticating={props.isAuthenticating}
            />
        </Container>
    );
}



export default LoginPageContainer;
