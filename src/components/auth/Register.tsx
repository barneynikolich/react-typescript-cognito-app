import * as React from 'react';
import { Button, TextField } from "@material-ui/core";
import { Auth } from 'aws-amplify';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

export class Register extends React.Component<any> {

    constructor(props: any) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    state = {
        username: "",
        email: "",
        password: "",
        confirmPassword: "",
        errors: {
            cognito: null,
            passwordMatch: false,
        }
    }

    private clearErrorState(): void {
        this.setState({
            errors: {
                cognito: {},
                passwordMatch: false
            }
        })
    }

    private async handleSubmit(event: React.FormEvent) {
        event.preventDefault();

        // Handle form validation
        //Assume is valid for now

        const { username, email, password, confirmPassword } = this.state;

        if (password !== confirmPassword) {
            console.log(password, confirmPassword)
            this.setState({
                errors: {
                    cognito: {
                        message: 'Passwords do not match'
                    }
                }
            })
        } else {
            // AWS Cognito integration
            try {
                console.log('Trying to auth... ', username, password, email)
                const signUpResponse = await Auth.signUp({
                    username,
                    password,
                    attributes: {
                        email
                    }
                });
                console.log('Sign up response: ', signUpResponse)
                this.props.onRegister(username);
            } catch (error) {
                console.log('error happening!')
                console.log(error)
                let err = null;
                err !== null ? err = { "message": error } : err = error
                this.setState({
                    errors: {
                        cognito: error
                    }
                })
            }
        }
    }

    private onInputChange = (event: any) => {
        this.setState({
            [event.target.id]: event.target.value
        });

    }

    private showError(error: any) {
        if (error) {
            return (
                <Typography variant="h6" style={{ color: 'red' }} >
                    <br />
                    {error.message}
                </Typography>
            )
        }
        return null;
    }

    render() {

        return (
            <Container className='login-container' maxWidth="sm">

                <form className="form-container" onSubmit={this.handleSubmit}>
                    <h1>Register</h1>
                    <div className="input-fields-container">
                        <TextField
                            id="username"
                            type="text"
                            label="Username"
                            margin="dense"
                            variant="outlined"
                            name="username"
                            value={this.state.username}
                            onChange={this.onInputChange}
                        />

                        <TextField
                            id="email"
                            type="email"
                            label="Email"
                            margin="dense"
                            variant="outlined"
                            name="email"
                            value={this.state.email}
                            onChange={this.onInputChange}
                        />

                        <TextField
                            id='password'
                            label="Password"
                            type="password"
                            margin="dense"
                            variant="outlined"
                            name="password"
                            value={this.state.password}
                            onChange={this.onInputChange}
                        />

                        <TextField
                            id='confirmPassword'
                            label="Confirm Password"
                            type="password"
                            margin="dense"
                            variant="outlined"
                            name="confirmPassword"
                            value={this.state.confirmPassword}
                            onChange={this.onInputChange}
                        />

                        <Button type="submit" id="login-button" variant="contained" color="primary">
                            Register
                    </Button>
                        {this.showError(this.state.errors.cognito)}
                    </div>
                </form>
            </Container>
        )
    }
}

export default Register;

