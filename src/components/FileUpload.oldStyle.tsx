import React, { useState } from 'react';

interface State {
    file: File | undefined
}

export class FileUpload extends React.Component<{}, State> {
    public state: State;

    constructor(props: any) {
        super(props);
        this.state = { file: undefined };
        this.uploadFile = this.uploadFile.bind(this);
    }
    handleChange = (fileList: FileList | null) => {
        if (fileList) {
            const inputFile: File = fileList[0];
            this.setState({ file: inputFile })
        }
    }

    cancelUpload = () => {
        this.setState({ file: undefined })
    }

    private uploadFile(): void {
        const fileName: any = this.state.file ? this.state.file.name : null;
        var request: XMLHttpRequest = new XMLHttpRequest();
        request.open('POST', 'http://localhost:8080/upload', true);
        request.send(fileName);
        request.onload = function (event) {
            const responseObj: string = request.response;
            console.log(responseObj)
        }

    }

    render() {
        return (
            <div className="govuk-width-container app-site-width-container">

                <h3 className="govuk-heading-l">Upload a file to perform analysis</h3>
                <div className="govuk-form-group">
                    <label className="govuk-label">
                        Upload a file
                </label>
                    <input onChange={(e) => this.handleChange(e.target.files)} className="govuk-file-upload" id="file-upload-1" name="file-upload-1" type="file"></input>
                </div>

                {this.state.file &&
                    <dl className="govuk-summary-list">
                        <div className="govuk-summary-list__row">
                            <dt className="govuk-summary-list__key">
                                Name
                        </dt>
                            <dd className="govuk-summary-list__value">
                                {this.state.file.name}
                            </dd>
                        </div>
                        <div className="govuk-summary-list__row">
                            <dt className="govuk-summary-list__key">
                                Type
                            </dt>
                            <dd className="govuk-summary-list__value">
                                {this.state.file.type}
                            </dd>
                        </div>
                        <div className="govuk-summary-list__row">
                            <dt className="govuk-summary-list__key">
                                Size
                        </dt>
                            <dd className="govuk-summary-list__value">
                                {this.state.file.size} bytes
                            </dd>
                        </div>
                        <div className="govuk-summary-list__row">
                            <dt className="govuk-summary-list__key">
                                Last modified
                        </dt>
                            <dd className="govuk-summary-list__value">
                                {this.state.file.lastModified}
                            </dd>
                        </div>

                        <div className="govuk-grid-column-two-third">
                            <button onClick={this.uploadFile} style={{ marginTop: "20px" }} type="submit" data-prevent-double-click="true" className="govuk-button" data-module="govuk-button">
                                Confirm and send
                            </button>

                            <button type="submit" onClick={this.cancelUpload} className="govuk-button govuk-button--warning" data-module="govuk-button">
                                Cancel upload
                            </button>
                        </div>
                    </dl>
                }
            </div>
        )
    }
}

export default FileUpload;