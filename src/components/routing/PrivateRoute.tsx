import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import rules from '../../auth/rbac-rules';

interface IPrivateRouteProps {
    isLoggedIn: boolean,
    props: {},
    redirectUrl: string,
    component: any
    path: string
    requiredRole: string,
    currentRole: string
}

export const PrivateRoute = ({ isLoggedIn, props, redirectUrl, currentRole, requiredRole, component, ...rest }: IPrivateRouteProps) => {
    return (
        < Route {...rest} render={() => {
            if (rest.path === '/login' || rest.path === '/register') {
                return isLoggedIn ?
                    React.createElement(component, props)
                    : <Redirect to={redirectUrl} />
            }

            const roles = rules[currentRole] ? rules[currentRole].static : [];
            if (roles && roles.indexOf(requiredRole) === -1) {
                return <Redirect to={redirectUrl} />
            }

            return React.createElement(component, props)
        }} />
    )
};

export default PrivateRoute;
