import * as React from 'react';

export class InfoComponent extends React.Component<{}, {}> {

    state = {
        info: ''
    }


    componentDidMount() {

        const that = this;
        var request: XMLHttpRequest = new XMLHttpRequest();
        request.open('GET', 'http://localhost:8080/info', true);
        request.send();
        request.onload = function (event) {
            const responseObj: string = request.response;
            that.setState({ info: responseObj });
        }
    }

    render() {
        return (
            <div>
                <h1>Info Component</h1>
                {
                    this.state.info &&
                    <p>
                        {this.state.info}
                    </p>
                }
            </div >
        )
    }
}
