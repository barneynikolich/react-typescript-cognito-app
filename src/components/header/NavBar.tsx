import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Clock from '../Clock';
import './NavBar.css';
import Can from '../../auth/Can';
import { CognitoUser } from '@aws-amplify/auth';
import { GovUkBanner } from './GovUkBanner';

interface Props {
    loggedIn: boolean;
    user: CognitoUser | null;
    userRole: string;
    handleLogout: () => void;
}

interface NavLinkProps {
    link: string,
    linkName: string
}

const NavLink: React.SFC<NavLinkProps> = (props) => {
    return (
        <Link to={props.link}>
            <Typography variant="h5">
                {props.linkName}
            </Typography>
        </Link>
    )
}

export const NavBar: React.FunctionComponent<Props> = (props) => {

    function handleLogoutClick(): void {
        props.handleLogout();
    }

    function getLoginButton(loggedIn: boolean): any {
        if (loggedIn) {
            return <Button onClick={handleLogoutClick} style={{ marginLeft: 'auto' }} color="inherit">Logout</Button>
        }
        return (
            <Link to="/login" style={{ textDecoration: 'none', color: 'white' }}>
                <Button style={{ marginLeft: 'auto' }} color="inherit">Login</Button>
            </Link>)
    }

    return <AppBar position="sticky" style={{ marginBottom: "50px" }} >
        <Toolbar style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#1d70b8' }}>
            <div className="nav-items-left">
                <NavLink link="/" linkName="Home" />
                <Can role={props.userRole}
                    perform='secret:visit'
                    yes={() => <NavLink link="/secret" linkName="Secret" />}
                />
                <Can role={props.userRole}
                    perform='user-management:visit'
                    yes={() => <NavLink link="/user-management" linkName="User Management" />}
                />
            </div>
            <div className="nav-items-centre">
                <Clock />
            </div>
            <div className="nav-items-right">
                {getLoginButton(props.loggedIn)}
            </div>
        </Toolbar>
        <GovUkBanner />
    </AppBar>
}