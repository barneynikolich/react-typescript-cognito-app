import * as React from 'react';
import '../../../node_modules/govuk-frontend/govuk/components/button/_button.scss'
import '../../../node_modules/govuk-frontend/govuk/all.scss'
import './Home.css';
import { Link } from 'react-router-dom';
import Can from '../../auth/Can';

interface HomeProps {
    userRole: string
}

interface StartButtonProps {
    url: string,
    content: string
}

const StartButton: React.SFC<StartButtonProps> = (props) => {
    return (
        <Link to={props.url} role="button" draggable={false} className="govuk-button app-button--inverted govuk-!-margin-top-6 govuk-!-margin-bottom-0 govuk-button--start" data-module="govuk-button">
            {props.content}
            <svg className="govuk-button__start-icon" xmlns="http://www.w3.org/2000/svg" width="17.5" height="19" viewBox="0 0 33 40" role="presentation" focusable="false">
                <path fill="currentColor" d="M0 0h13l20 20-20 20H0l20-20z" />
            </svg>
        </Link>)
}

export const HomePage: React.FC<HomeProps> = (props) => {
    return (
        <div>
            <div className="app-masthead">
                <div className="govuk-width-container app-site-width-container">
                    <div className="govuk-grid-row">
                        <div className="govuk-grid-column-two-thirds">
                            <h1 className="govuk-heading-xl app-masthead__title">Design your service using GOV.UK styles, components and&nbsp;patterns</h1>
                            <p className="app-masthead__description"> This page has been designed with GDS Styles and components.
                             Click get started to upload a file.</p>

                            <Can
                                role={props.userRole}
                                perform="fileUpload:visit"
                                yes={() => <StartButton url="/fileUpload" content="Get started" />}
                                no={() => <StartButton url="/login" content="Please log in to upload a file" />}
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div className="govuk-grid-row">
                <div className="govuk-grid-column-two-thirds" style={{ paddingLeft: "50px" }}>
                    <p>This is a govuk-grid-column-two-thirds</p>
                    <div className="govuk-inset-text">
                        It can take up to 8 weeks to register a lasting power of attorney if there are no mistakes in the application.
                    </div>
                </div>
                <div className="govuk-grid-column-one-third">
                    <div className="govuk-warning-text">
                        <span className="govuk-warning-text__icon" aria-hidden="true">!</span>
                        <strong className="govuk-warning-text__text">
                            <span className="govuk-warning-text__assistive">Warning</span>
                            You can be fined up to £5,000 if you do not register.
                        </strong>
                    </div>
                </div>
            </div>

        </div >
    );
};

export default HomePage;