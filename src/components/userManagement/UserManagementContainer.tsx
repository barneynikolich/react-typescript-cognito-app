import * as React from 'react';
import { UserManagementActionBar } from './UserManagementActionBar';
import UserList from './UserList';
import { UserData } from './models/User';
import { userService } from '../../services/User.service';

interface IState {
    users: UserData[]
}

export class UserManagementContainer extends React.Component<{}, IState> {

    public state: IState;

    constructor(props: any) {
        super(props);
        this.state = { users: [] };
    }

    componentDidMount() {
        userService.getAllUsers().then((data) => {
            this.setState({ users: data })
        });
    }

    render() {
        return (
            <div className="govuk-width-container app-site-width-container">
                <UserManagementActionBar />
                <UserList users={this.state.users} />
            </div>
        )
    }
}