import * as React from 'react';
import './User.css';
import { UserData } from '../models/User';

interface UserProps {
    user: UserData;
}

export function User(props: { user: UserData }) {
    return (
        <div className="user-container">
            <h1>{props.user.name}</h1>
        </div>
    )
}
