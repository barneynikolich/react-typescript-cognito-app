export interface UserData {
    username: string,
    email: string,
    role: string,
    name: string
}