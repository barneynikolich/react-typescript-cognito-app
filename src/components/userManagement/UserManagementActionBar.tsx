import * as React from 'react';
import './UserManagementActionBar.css';
import Button from '@material-ui/core/Button';

export const UserManagementActionBar: React.SFC<{}> = (props) => {

    return (
        <div className="action-bar-container">
            <Button variant="outlined" color="primary">
                Add User
            </Button>
        </div >
    )
}