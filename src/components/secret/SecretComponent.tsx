import * as React from 'react';
import LockOpenIcon from '@material-ui/icons/LockOpenTwoTone'
import Grow from '@material-ui/core/Grow';
// import { makeStyles } from '@material-ui/core/styles';


// const useStyles = makeStyles(theme => ({
//   root: {
//     height: 180,
//   },
//   container: {
//     display: 'flex',
//   },
//   paper: {
//     margin: theme.spacing(1),
//   },
//   svg: {
//     width: 100,
//     height: 100,
//   },
//   polygon: {
//     fill: theme.palette.common.white,
//     stroke: theme.palette.divider,
//     strokeWidth: 1,
//   },
// }));
export const SecretComponent: React.FC = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <Grow in={true} style={{ transformOrigin: '0 0' }} {...(true ? { timeout: 2500 } : {})}>
        <LockOpenIcon style={{ fontSize: '30rem', marginTop: '200px' }}></LockOpenIcon>
      </Grow>
    </div>
  );
};
