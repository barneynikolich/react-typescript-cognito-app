import React, { useState } from 'react';

interface State {
    file: File | undefined
}


const GovUkDetailRow: React.FC<{ title: string, value: string | number }> = (props) => {
    return (
        <div className="govuk-summary-list__row">
            <dt className="govuk-summary-list__key">
                {props.title}
            </dt>
            <dd className="govuk-summary-list__value">
                {props.value}
            </dd>
        </div>
    )
}

const GovUkDetailList: React.FunctionComponent<{ detailRows: any[] }> = (props) => {
    return (
        <dl className="govuk-summary-list">
            {props.detailRows.map((detail) => {
                return <GovUkDetailRow key={detail.props.title} title={detail.props.title} value={detail.props.value} />
            })}
        </dl>
    )
}

export const FileUpload: React.FC<{}> = (props) => {
    const [file, setFile] = useState<File | undefined>();

    function handleChange(fileList: FileList | null): void {
        if (fileList) {
            setFile(fileList[0]);
        }
    }

    function cancelUpload(): void {
        setFile(undefined);
    }

    function uploadFile(): void {
        const fileName: any = file ? file.name : null;
        const request: XMLHttpRequest = new XMLHttpRequest();
        request.open('POST', 'http://localhost:8080/upload', true);
        request.send(fileName);
        request.onload = function (event) {
            const responseObj: string = request.response;
            console.log(responseObj)
        }
    }

    return (
        <div className="govuk-width-container app-site-width-container">

            <h3 className="govuk-heading-l">Upload a file to perform analysis</h3>
            <div className="govuk-form-group">
                <label className="govuk-label">
                    Upload a file
            </label>
                <input onChange={(e) => handleChange(e.target.files)} className="govuk-file-upload" id="file-upload-1" name="file-upload-1" type="file" />
            </div>

            {file &&
                <>
                    <GovUkDetailList detailRows={
                        [
                            <GovUkDetailRow key={file.name} title="Name" value={file.name} />,
                            <GovUkDetailRow key={file.name} title="Type" value={file.type} />,
                            <GovUkDetailRow key={file.name} title="Size" value={file.size} />,
                            <GovUkDetailRow key={file.name} title="Last modified" value={file.lastModified} />
                        ]
                    } />

                    <div className="govuk-grid-column-two-third">
                        <button onClick={uploadFile} style={{ marginRight: "20px" }} type="submit" data-prevent-double-click="true" className="govuk-button" data-module="govuk-button">
                            Confirm and send
                        </button>

                        <button type="submit" onClick={cancelUpload} className="govuk-button govuk-button--warning" data-module="govuk-button">
                            Cancel upload
                        </button>
                    </div>
                </>
            }
        </div>
    )
}
