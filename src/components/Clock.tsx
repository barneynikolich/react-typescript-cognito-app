import * as React from 'react';
import Typography from '@material-ui/core/Typography';

export default class Clock extends React.Component {

    public state = {
        date: new Date()
    }

    public timerId: any;

    componentDidMount() {
        this.timerId = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    private tick(): void {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <Typography variant="h6">
                {this.state.date.toLocaleTimeString()}
            </Typography>
        )
    }
}




