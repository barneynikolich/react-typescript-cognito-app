const rules: any = {
    visitor: {
        static: ["posts:list", "home-page:visit"]
    },
    admin: {
        static: [
            "home-page:visit",
            "secret:visit",
            "user-management:visit",
            "fileUpload:visit"
        ]
    },
    user: {
        static: [
            "home-page:visit",
            "secret:visit",
            "fileUpload:visit"
        ]
    }
};

export default rules;