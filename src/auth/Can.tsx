import rules from "./rbac-rules";

const check = (rules: any, role: any, action: any, data: any) => {
    const permissions = rules[role];

    // Check if role even exists in the rules
    if (!permissions) {
        return false;
    }

    const staticPermissions = permissions.static;

    if (staticPermissions && staticPermissions.includes(action)) {
        // rule does not exist
        return true;
    }

    return false;
};

const Can = (props: any) =>
    check(rules, props.role, props.perform, props.data)
        ? props.yes()
        : props.no();

Can.defaultProps = {
    yes: () => null,
    no: () => null
};

export default Can;